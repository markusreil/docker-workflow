import sys

__author__ = 'mreil'

from subprocess import call, Popen, PIPE
from util import regex
import json
import os.path
import requests


def __execute(path, args=None):
    return call(["docker %s" % args],
                shell=True,
                cwd=path)


def build(path, image_id):
    res = __execute(path, "build -t %s ." % image_id)
    if res != 0:
        sys.exit("Build failed")

    return res


def tag(image_id, tag, force=False):
    f = "-f" if force else ""
    res = __execute(None, "tag %s %s %s" % (f, image_id, tag))
    if res != 0:
        sys.exit("Tag failed")


def rmi(image_id):
    res = __execute(None, "rmi %s" % image_id)


def inspect(name):
    p = Popen(["docker", "inspect", "%s" % name],
              stdout=PIPE)
    out = p.communicate()[0]
    if p.returncode != 0:
        raise Exception("No image/container %s" % name)
    return json.loads(out)


def extract_env_vars(image_id):
    data = inspect(image_id)

    env = data[0]['ContainerConfig']['Env']
    ret = {}
    for e in env:
        for m in regex.match("^(DOCKER_.*)=(.*)", e):
            ret[m.group(1)] = m.group(2)
    print "Found env: %s" % ret
    return ret


def exists_dockerfile(path):
    return os.path.isfile("%s/Dockerfile" % path)


def repo_tag_exists(name, version):
    tags_url = "https://index.docker.io/v1/repositories/%s/tags" % name
    r = requests.get(tags_url)
    if r.status_code == 404:
        return False
    versions = r.json()
    for v in versions:
        if v['name'] == version:
            return True
    return False


def local_tag_exists(name, version):
    try:
        inspect("%s:%s" % (name, version))
        return True
    except:
        return False
