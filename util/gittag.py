from subprocess import call

__author__ = 'mreil'


def __execute(path, args=None):
    return call(["git %s" % args],
                shell=True,
                cwd=path)


def tag_exists(path, git_tag):
    return __execute(path, "tag -l %s | grep %s" % (git_tag, git_tag)) == 0


def tag(path, git_tag):
    return __execute(path, "tag %s" % git_tag)
