#!/usr/bin/python

import argparse
import os
import uuid
import sys
import re
from docker import docker
from util import gittag

parser = argparse.ArgumentParser(description='Build a Docker image.')
parser.add_argument('path', metavar='PATH', nargs='?', default=os.getcwd(),
                    help='Path of the Dockerfile')
parser.add_argument('-t', dest='tag', action='store_true',
                    help='Path of the Dockerfile')

args = parser.parse_args()
dir = args.path
image_id = "build-%s" % uuid.uuid4()

if not docker.exists_dockerfile(dir):
    sys.exit("No Dockerfile at %s" % dir)

docker.build(dir, image_id)

env = docker.extract_env_vars(image_id)
name = env['DOCKER_NAME']
version = env['DOCKER_VERSION']
date = env['DOCKER_BUILD_DATE']
git_tag = "%s/%s" % (re.split('/', name)[-1], version)

local_exists = docker.local_tag_exists(name, version)
hub_exists = docker.repo_tag_exists(name, version)
git_tag_exists = gittag.tag_exists(dir, git_tag)

print "Built: %s" % name
print "Current Version: %s" % version

if git_tag_exists:
    print "Warning: Git tag %s already exists" % git_tag
if local_exists:
    print "Warning: Tag %s already exists in local repository" % version
if hub_exists:
    print "Warning: Tag %s already exists in hub.docker.io" % version

tag_exists = local_exists or hub_exists or git_tag_exists

docker.tag(image_id, name, True)
docker.rmi(image_id)

if not args.tag and not tag_exists:
    print "Re-run with -t switch to tag this build"

if args.tag:
    if tag_exists:
        sys.exit(1)
    docker.tag("%s:latest" % name, "%s:%s" % (name, version))
    gittag.tag(dir, git_tag)
